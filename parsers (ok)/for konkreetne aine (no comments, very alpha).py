#pip install requests, beautifulsoup4, lxml
import requests
import bs4
#kui ei importi toppelt, siis \def convert_soup_to_text(names):
#"NameError: name 'bs4' is not defined"
from bs4 import BeautifulSoup

def get_html(url):
    r = requests.get(url) 
    return r.text     
    
def get_all(html):
    soup = BeautifulSoup(html, 'lxml')
    all_TRs = soup.find('table').find_all('tr')
    
    name_list = []
    grade_list = []
    range_list = []
    feedback_list = []

    for i in all_TRs:
        
        name = i.find('th')
        name_list.append(name)
        
        grade = i.find('td', class_ = "column-grade")
        grade_list.append(grade)
        
        range_ = i.find('td', class_ = "column-range")
        range_list.append(range_)
        
        feedback = i.find('td', class_ = "column-feedback")
        feedback_list.append(feedback)
        
    return name_list, grade_list, range_list, feedback_list
 
def convert_soup_to_text(list_):
    new_list = []
    
    for i in list_:
        if isinstance(i, bs4.element.Tag):
            x = i.text
            new_list.append(x)
        else:  #kui see on NoneType soupi jaoks, ekh lihtsalt tyhi jupp
            new_list.append('-')
     
    return new_list

html1 = open("prog.html", encoding = 'utf-8')
html2 = open("ara.html", encoding = 'utf-8')

names1, grades1, ranges1, feedbacks1 = get_all(html1)
names2, grades2, ranges2, feedbacks2 = get_all(html2)

names1 = convert_soup_to_text(names1)
grades1 = convert_soup_to_text(grades1)
names2 = convert_soup_to_text(names2)
grades2 = convert_soup_to_text(grades2)

d1 = {}
for i in range(len(names1)):
    x1 = names1[i]
    y1 = grades1[i]
    d1[x1] = y1
    
d2 = {}
for i in range(len(names2)):
    x2 = names2[i]
    y2 = grades2[i]
    d2[x2] = y2

f1 = open('prog.txt', 'w', encoding = 'utf-8')
for n in d1:
    f1.write(n + " : " + d1[n])
    f1.write('\n')
f2 = open('ara_tulemused.txt', 'w', encoding = 'utf-8')
for m in d2:
    f2.write(m + " : " + d2[m])
    f2.write('\n')

f1.close()
f2.close()
    
    



