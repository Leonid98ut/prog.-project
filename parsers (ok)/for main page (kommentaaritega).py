html = open("hinded.html")


#pip install requests, beautifulsoup4, lxml
import requests
from bs4 import BeautifulSoup

def get_html(url):
    r = requests.get(url) #response object
    return r.text     #tagastab html from url
    
def get_all_links(html):
    
    soup = BeautifulSoup(html, 'lxml') #html textina
    
    links = []
    names = []
    grades = []
    comprasions = []
    
    all_names = soup.find('table', id="overview-grade").find_all('td', class_="cell c0") #html objekt, tegiga TD, classiga CELL C0 (seen veebis html koodis sain)
    all_grades = soup.find('table', id="overview-grade").find_all('td', class_="cell c1")
    all_comprasions = soup.find('table', id="overview-grade").find_all('td', class_="cell c2")
    
    for course_name in all_names:  #igale TD elementile tekstis (mite paris tekstis, vaid html koodis, ehk soupi objektis) (ehk see html koodi jupp ei ole paris str)
        if course_name.text != "":  #kui TEKSTI sisu on olemas / ekh ".text" konverteerib elementi sisu tekstile
            names.append(course_name.text)  #votab tekstina ainete nimi 
            link = course_name.find('a').get('href') #votab ainete link (see 'get' juba tagastab str type, pole vaja .text teha et str saada)
            links.append(link)
            
    for course_grade in all_grades: # sama vaid hinnadele jaoks
        if course_grade.text != "": #muide, kui ma ei tee seda, seal on 6 tais ainet, ja mingi 29 tyhiu jooned selle TD tegiga, ja Py tagastab mingi NONE type
            grades.append(course_grade.text)
    
    for course_comprasion in all_comprasions:
        if course_comprasion.text != "":
            comprasions.append(course_comprasion.text)
            
    return names, links, grades, comprasions  #saime kooraga neeli mutajad
    

names, links, grades, comprasions = get_all_links(html) #hetkel html failist, aga saab module requests teha, et saada HTML kood veebist