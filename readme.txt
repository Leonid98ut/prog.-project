modules:
fake_user_agent
requests
bs4
selenium!

software for silenium:
geckodriver https://github.com/mozilla/geckodriver/releases (vastavalt op süsteemi)
chromedriver https://chromedriver.chromium.org/downloads
(viskasin webdriverid lihtsalt projekti folderi)

selles projektis mugavam kasutata firefox, et saaks config seal hästi palju muuta.
Edaspidi proovime ikka chromi jaoks optümiseerida ka.